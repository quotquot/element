import { terser } from "rollup-plugin-terser";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";

export default {
    input: "index.mjs",
    output: {
        file: "dist/index.mjs",
        format: "esm"
    },
    external: ["logging"],
    plugins: [
        commonjs(),
        resolve({ browser: true }),
        process.env.BUILD === "production" && terser()
    ]
};
