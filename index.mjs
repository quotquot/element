/*
 * Copyright 2020-2021 Johnny Accot
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *      http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import logging from "logging";

const logger = logging.getLogger("@quotquot/element");
// logger.setLevel("DEBUG");

if (!Set.prototype.isDisjointFrom)
    Set.prototype.isDisjointFrom = function(other) {
        return !Array.from(other).some(x => this.has(x));
    };

export class ShadowPlugin {

    #mode;
    #shadowRoot;

    constructor(mode) {
        this.#mode = mode;
    }

    get name() {
        return "shadow";
    }

    get requires() {
        return [];
    }

    async setup(el, plugins) {
        this.#shadowRoot = el.attachShadow({ mode: this.#mode });
        Object.defineProperties(el, {
            $: {
                value: selector =>  this.#shadowRoot.querySelector(selector),
                enumerable: false,
                writable: false,
                configurable: false
            },
            $$: {
                value: selector =>  this.#shadowRoot.querySelectorAll(selector),
                enumerable: false,
                writable: false,
                configurable: false
            }
        });
    }

    async cleanup(el, plugins) {
        // TODO: delete $/$$ properties
    }

    get root() {
        return this.#shadowRoot;
    }
}

export class State {

    #state;
    #observers;

    constructor(state) {
        this.#state = state || {};
        this.#observers = [];
    }

    get(name) {
        return this.#state[name];
    }

    set(name, value) {
        this.#state[name] = value;;
    }

    getAll() {
        return this.#state;
    }

    addObserver(observer) {
        this.#observers.push(observer);
    }

    removeObserver(observer) {
        const index = this.#observers.indexOf(observer);
        if (index !== -1)
            this.#observers.splice(index, 1);
    }

    /**
     * Update the state of the element and update its rendering accordingly
     * @param {Object} changes The changes in the state of the manager
     * @returns {void}
     */
    async update(changes) {
        const newState = Object.assign({}, this.#state, changes);
        const changed = new Set(Object.keys(changes));
        await Promise.all(
            this.#observers.map(observer => observer.onUpdate(this, this.#state, newState, changed).catch(logger.error))
        );
        this.#state = newState;
    }

    /**
     * Toggle the state of a variable of the element state
     * @param {string} variable the name of the variable whose state must be toggled
     * @returns {void}
     */
    async toggle(variable) {
        await this.update({ [variable]: !this.#state[variable] });
    }

    /**
     * Push data into an array in the element state
     * @param {string} variable the array of the element state to be modified
     * @param {*} item the item to be pushed
     * @returns {void}
     */
    async push(variable, item) {
        const array = Array.from(this.#state[variable]);
        array.push(item);
        await this.update({ [variable]: array });
    }
}

export class StatePlugin {

    constructor(state) {
        this.wrapped = new State(state);
    }

    get name() {
        return "state";
    }

    get requires() {
        return ["shadow"];
    }

    async setup(el, plugins) {
        el.shadowRoot.state = this.wrapped;
    }

    async cleanup(el, plugins) {
        el.shadowRoot.state = null;
    }
}

export class AttrRenderer {

    #handlers;
    #AttrHandler;

    constructor(AttrHandler) {
        this.#AttrHandler = AttrHandler;
    }

    get name() {
        return this.constructor.name;
    }

    async setup(root, state) {
        this.#handlers = new Map();
    }

    async cleanup(root, state) {
        /** @note a WeakMap would have been better than Map for this.#handlers
         * but it does not allow enumeration, and doesn't have a `clear` method */
        for (const handler of this.#handlers.values()) {
            handler.clear();
            handler.unbind();
        }
        this.#handlers = null;
    }

    async render(el, parent, manager, newState, context) {
        const attrs = this.#AttrHandler.getAttributeNodes(el);
        if (attrs) {
            let handler = this.#handlers.get(el);
            if (!handler) {
                handler = new this.#AttrHandler(this, el, parent, attrs);
                this.#handlers.set(el, handler);
            }
            await handler.render(manager, newState, context);
        }
    }
}

export class RendererPlugin {

    constructor(Renderer) {
        this.wrapped = new Renderer();
    }

    get name() {
        return "renderer";
    }

    get requires() {
        return ["shadow", "state", "rendering"];
    }

    async setup(el, plugins) {
        plugins.rendering.wrapped.addRenderer(this.wrapped);
        await this.wrapped.setup(plugins.shadow.root, plugins.state.wrapped);
    }

    async cleanup(el, plugins) {
        await this.wrapped.setup(plugins.shadow.root, plugins.state.wrapped);
        plugins.rendering.wrapped.removeRenderer(this.wrapped);
    }
}

export class BaseHandler {

    #parent;
    #context;

    constructor(parent) {
        if (parent) {
            this.#parent = parent;
            this.#parent.addChild(this);
        }
    }

    getContext(context) {
        return Object.assign({}, this.#context, context);
    }

    setContext(context) {
        this.#context = Object.assign({}, context);
    }

    getValues(newState) {
        return Object.assign({}, newState, this.#context);
    }

    unbind() {
        // TODO: this.#renderer.unbind(this);
        if (this.#parent)
            this.#parent.removeChild(this);
    }
}

export class BaseTemplateHandler extends BaseHandler {

    #nodes;
    #children;

    constructor(parent) {
        super(parent);
        this.#nodes = [];
        this.#children = new Set();
    }

    async update(manager, oldState, newState, changed) {
        await Promise.all(
            Array.from(this.#children).map(
                child => child.update(manager, oldState, newState, changed).catch(logger.error)
            )
        );
    }

    async instantiate(content, manager, newState, newContext) {
        const fragment = content.cloneNode(true);
        await manager.renderFragment(fragment, this, newState, this.getContext(newContext));
        this.#nodes.push(...fragment.childNodes);
        return fragment;
    }

    clear() {
        for (const child of this.#children) {
            child.clear();
            child.unbind();
        }
        this.#children.length = 0;
        for (const node of this.#nodes)
            // TODO: parentNode seems to already be null for some nodes, investigate
            node.remove();
        this.#nodes.length = 0;
    }

    addChild(handler) {
        this.#children.add(handler);
    }

    removeChild(handler) {
        this.#children.delete(handler);
    }
}

/** @todo use Proxy for state?
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy
 */

export class RenderingManager extends BaseTemplateHandler {

    #root;
    #renderers;

    // TODO: add element version for renderer compatibility

    /**
     * Constructs an element renderer
     * @param {HTMLElement} root The element that must be managed
     * @param {HTMLTemplateElement} template the initial manager template
     * @param {Object} state initial state of the element
     * @param {Array} renderers external behavior that will be merged into the element
     */
    constructor() {
        super(/* no parent */);
        this.#renderers = [];
    }

    addRenderer(renderer) {
        this.#renderers.push(renderer);
    }

    removeRenderer(renderer) {
        const index = this.#renderers.indexOf(renderer);
        if (index !== -1)
            this.#renderers.splice(index, 1);
    }

    /**
     * Clone a template and append the content to the shadow DOM.
     * @param {HTMLTemplateElement} template the template to be cloned
     * @returns {void}
     */
    async appendTemplate(template, newState) {
        this.#root.appendChild(await this.instantiate(template.content, this, newState, {}));
    }

    async setup(root, state) {
        this.#root = root; /** @todo WeakRef */
        state.addObserver(this);
    }

    async cleanup(root, state) {
        state.removeObserver(this);
        this.#root = null;
    }

    /**
     * Update the state of the element and update its rendering accordingly
     * @param {Object} changes The changes in the state of the manager
     * @returns {void}
     */
    async onUpdate(state, oldState, newState, changed) {
        await Promise.all(
            this.#renderers.map(
                renderer => renderer.onBeforeUpdate &&
                    renderer.onBeforeUpdate(this.#root, state, newState, changed).catch(logger.error)
            )
        );

        await super.update(this, oldState, newState, changed);

        /* notice interested renderers that the update has been completed, or failed */
        await Promise.all(
            this.#renderers.map(
                renderer => renderer.onAfterUpdate &&
                    renderer.onAfterUpdate(this.#root, state, newState, changed).catch(logger.error)
            )
        );
    }

    /**
     * Render a node given a state
     * @param {Element} el the root of the subtree in the shadow DOM to be rendered
     * @param {Object} values the new element state that will be used in rendering
     * @param {Object} changed the changes between the current state and |values|
     * @returns {void}
     */
    async #render(el, parent, newState, context) {
        const walker = document.createTreeWalker(el, NodeFilter.SHOW_ELEMENT);
        do {
            await Promise.all(
                this.#renderers.map(
                    renderer => renderer.render(walker.currentNode, parent, this, newState, context).catch(logger.error)
                )
            );
        } while (walker.nextNode());
    }

    async renderFragment(fragment, parent, newState, context) {
        /* new elements can be added to `fragment`; we copy the child list to be sure
         * we render only the initial elements */
        await Promise.all(
            Array.from(fragment.children).map(el => this.#render(el, parent, newState, context))
        );
    }
}

export class RenderingPlugin {

    constructor(Renderers) {
        this.wrapped = new RenderingManager(Renderers);
    }

    get name() {
        return "rendering";
    }

    get requires() {
        return ["shadow", "state"];
    }

    async setup(el, plugins) {
        await this.wrapped.setup(plugins.shadow.root, plugins.state.wrapped);
    }

    async cleanup(el, plugins) {
        await this.wrapped.setup(plugins.shadow.root, plugins.state.wrapped);
    }
}

export class TemplatePlugin {

    #template;

    constructor(template) {
        this.#template = template;
    }

    get name() {
        return "template";
    }

    get requires() {
        return ["rendering", "state", "renderer"];
    }

    async setup(el, plugins) {
        await plugins.rendering.wrapped.appendTemplate(this.#template, plugins.state.wrapped.getAll());
    }

    async cleanup(el, plugins) {
        plugins.manager.wrapped.clear();
    }
}

export class PluginElement extends HTMLElement {

    #plugins;
    #ordered;

    constructor(plugins) {
        super();
        // https://stackoverflow.com/questions/49875255/
        this.#ordered = plugins.sort(
            (a, b) => a.requires.includes(b.name) ? 1 : b.requires.includes(a.name) ? -1 : 0
        );
        logger.debug(this.localName, "ordered plugins:", this.#ordered.map(plugin => plugin.name));
        this.#plugins = Object.fromEntries(this.#ordered.map(plugin => [plugin.name, plugin]));
    }

    async setup() {
        for (const plugin of this.#ordered)
            await plugin.setup(this, this.#plugins);
    }

    async cleanup() {
        for (const plugin of this.#ordered.toReversed())
            await plugin.cleanup(this, this.#plugins);
    }

    /**
     * Connexion callback, used to set up renderers.
     * Subclasses must call this superclass method if they override the method.
     * @returns {void}
     */
    connectedCallback() {
        this.setup().catch(logger.error);
    }

    /**
     * Disconnexion callback, used to clean up renderers.
     * Subclasses must call this superclass method if they override the method.
     * @returns {void}
     */
    disconnectedCallback() {
        this.cleanup().catch(logger.error);
    }
}

export class QuotquotElement extends PluginElement {
    constructor({ mode="open", state={}, plugins, Plugins, template, Renderers }) {
        const elementPlugins = [
            new ShadowPlugin(mode),
            new StatePlugin(state)
        ];
        if (plugins)
            elementPlugins.push(...plugins);
        if (Plugins)
            elementPlugins.push(...Plugins.map(Plugin => new Plugin()));
        if (Renderers) {
            elementPlugins.push(
                new RenderingPlugin(),
                ...Renderers.map(Renderer => new RendererPlugin(Renderer))
            );
            if (template)
                elementPlugins.push(new TemplatePlugin(template));
        }
        super(elementPlugins);
    }
}
