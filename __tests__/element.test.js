/* eslint require-jsdoc: off */

import QuotquotElement from "../index";

const template = document.createElement("template");
template.innerHTML = '<div></div>';

class Renderer {
    constuctor(manager) {
    }
    async setup (manager) {
    }
    async cleanup(manager) {
    }
}

class GoodQuotquotElement extends QuotquotElement {
    constructor() {
        super({ template, Renderers: [Renderer] });
    }
}

window.customElements.define("good-element", GoodQuotquotElement);

test("Create good-element element", async() => {

    const el = document.createElement("good-element");
    document.body.appendChild(el);

    expect(el.tagName).toBe("GOOD-ELEMENT");

    // TODO: test when an exception is thrown in a renderer setup

    // TODO: renderer cleanup should happen after setup is over. Have a renderer
    // whose setup waits for a second, while the element is created, connected to
    // the document, then immediately disconnected. The disconnection callback should
    // wait for the setup to be over before cleaning up. Also test multiple fast
    // appendChild/removeChild to be sure all setups/cleanups are done sequentially

    // TODO: test parallel updates
});
